package oose.dea.Playlist.Service;

import oose.dea.Playlist.DataSource.IPlaylistDAO;
import oose.dea.Playlist.DataSource.PlaylistDAO;
import oose.dea.Playlist.Domain.Playlist;
import org.junit.Test;
import org.mockito.InOrder;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;


public class ManagePlaylistsServiceTest {
    private IPlaylistDAO playlistDAO = mock(PlaylistDAO.class);
    private ManagePlaylistsService managePlaylistsService = new ManagePlaylistsService(playlistDAO);


    @Test
    public void testAddPlaylist(){
        InOrder inOrder = inOrder(playlistDAO);
        Playlist playlist = new Playlist("1", "niek", "mijnList");
        managePlaylistsService.addPlaylist(playlist, "niek");
        inOrder.verify(playlistDAO, times(1)).save(playlist, "niek");
    }

    @Test
    public void testGetPlaylists(){
        InOrder inOrder = inOrder(playlistDAO);
        managePlaylistsService.getPlaylists();
        inOrder.verify(playlistDAO, times(1)).list();
    }

    @Test
    public void testDeletePlaylist(){
        InOrder inOrder = inOrder(playlistDAO);
        managePlaylistsService.deletePlaylist("10", "niek");
        inOrder.verify(playlistDAO, times(1)).delete("10", "niek");
        managePlaylistsService.deletePlaylist("-1", "niek");
        inOrder.verify(playlistDAO, times(1)).delete("-1", "niek");
    }
}