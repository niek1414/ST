<%@ page import="oose.dea.Track.Domain.Track" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Spotitube</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

<table>
  <thead>
  <th></th>
  <th>Artiest</th>
  <th>Titel</th>
  <th>URL</th>
  <th>Duur</th>
  <th>Offline beschikbaar</th>
  </thead>
  <tbody>
  <c:forEach items="${all}" var="current">
    <tr>
      <td>
        <img src="<c:out value="${current.imageURL}"/>" width="100px" height="100px">
      </td>
      <td>
        <c:out value="${current.performer}"/>
      </td>
      <td>
        <c:out value="${current.title}"/>
      </td>
      <td>
        <a href="<c:out value="${current.url}"/>">In Spotify bekijken</a>
      </td>
      <td>
        <c:out value="${current.duration}"/>
      </td>
      <td>
        <c:out value="${current.availability}"/>
      </td>
      <td>
        <form method="get" action="/findTracksInPlaylist">
          <input type="hidden" name="playlist" value="${playlist}">
          <input type="hidden" name="availability" value="${current.availability}">
          <input type="submit" name="setAvailability" value="Switch beschikbaarheid">
        </form>
      </td>
    </tr>
  </c:forEach>
  </tbody>
</table>
<form action="/findPlaylists">
  <input class="back" type="submit" value="Ga terug">
</form>
</body>
</html>