<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Spotitube</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<form method="post" action="/findTrack" class="center500">
  <input type="text" name="search"> <input class="aj" type="submit" value="Zoek">
</form>

<table>
  <thead>
  <th></th>
  <th>Artiest</th>
  <th>Titel</th>
  <th>URL</th>
  <th>Duur</th>
  </thead>
  <tbody>
  <c:forEach items="${all}" var="current">
    <tr>
      <td>
        <img src="<c:out value="${current.imageURL}"/>" width="100px" height="100px">
      </td>
      <td>
        <c:out value="${current.performer}"/>
      </td>
      <td>
        <c:out value="${current.title}"/>
      </td>
      <td>
        <a href="<c:out value="${current.url}"/>">In Spotify bekijken</a>
      </td>
      <td>
        <c:out value="${current.duration}"/>
      </td>
      <td>
        <form method="post" action="/choosePlaylists">
          <input type="hidden" name="track" value="${current.idTrack}">
          <input type="submit" name="bind" value="Voeg toe aan playlist...">
        </form>
      </td>
    </tr>
  </c:forEach>
  </tbody>
</table>
<form action="menu.jsp">
  <input class="back" type="submit" value="Ga terug">
</form>
</body>
</html>