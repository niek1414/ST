<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Spotitube</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<form method="post" action="/addPlaylist" class="center500">
  <input type="text" name="playlistnaam"> <input class="aj" type="submit" value="Nieuwe playlist toevoegen">
</form>
<table>
  <thead>
  <th></th>
  <th>Eigenaar</th>
  <th>Naam</th>
  </thead>
  <tbody>
  <c:forEach items="${all}" var="current">
    <tr>
      <td>
        <img src="<c:out value="${current.imageURL}"/>" width="100px" height="100px">
      </td>
      <td>
        <c:out value="${current.owner}"/>
      </td>
      <td>
        <c:out value="${current.name}"/>
      </td>
      <td>
        <form method="post" action="/deletePlaylist"><input type="hidden" name="playlist" value="${current.idPlaylist}"><input type="submit" name="delete" value="Verwijder"></form>
      </td>
      <td>
        <form method="post" action="/updatePlaylist">
          <input type="hidden" name="playlist" value="${current.idPlaylist}">
          <p>
            <input type="text" name="name">
            <input class="aj" type="submit" value="playlist wijzigen">
          </p>
        </form>
      </td>
      <td>
        <form method="post" action="/findTracksInPlaylist">
          <input type="hidden" name="playlist" value="${current.idPlaylist}">
          <input type="hidden" name="owner" value="${current.owner}">
            <input type="submit" value="Tracks in deze playlist laten zien.">
        </form>
      </td>
    </tr>
  </c:forEach>
  </tbody>
</table>
<form action="menu.jsp">
  <input class="back" type="submit" value="Ga terug">
</form>
</body>
</html>
