package oose.dea.Guice;

import com.google.inject.servlet.ServletModule;
import oose.dea.Binder.DataSource.BinderDAO;
import oose.dea.Binder.DataSource.IBinderDAO;
import oose.dea.Binder.Presentation.BinderController;
import oose.dea.Binder.Presentation.FindTracksInPlaylistController;
import oose.dea.Binder.Service.*;
import oose.dea.Playlist.DataSource.IPlaylistDAO;
import oose.dea.Playlist.DataSource.PlaylistDAO;
import oose.dea.Playlist.Presentation.AddPlaylistController;
import oose.dea.Playlist.Presentation.DeletePlaylistController;
import oose.dea.Playlist.Presentation.FindPlaylistsController;
import oose.dea.Playlist.Presentation.UpdatePlaylistController;
import oose.dea.Playlist.Service.IManagePlaylistsService;
import oose.dea.Playlist.Service.ManagePlaylistsService;
import oose.dea.Track.DataSource.ITrackDAO;
import oose.dea.Track.DataSource.TrackDAO;
import oose.dea.Track.Presentation.ChoosePlaylistController;
import oose.dea.Track.Presentation.FindTrackController;
import oose.dea.Track.Service.ISearchTrackService;
import oose.dea.Track.Service.SearchTrackService;
import oose.dea.User.DataSource.IUserDAO;
import oose.dea.User.DataSource.UserDAO;
import oose.dea.User.Presentation.CallbackController;
import oose.dea.User.Presentation.LoginController;
import oose.dea.User.Service.ILoginService;
import oose.dea.User.Service.LoginService;

public class AppBinding extends ServletModule{
    @Override
    protected void configureServlets() {
        super.configureServlets();
        serve("/binder").with(BinderController.class);
        serve("/findTracksInPlaylist").with(FindTracksInPlaylistController.class);
        serve("/addPlaylist").with(AddPlaylistController.class);
        serve("/deletePlaylist").with(DeletePlaylistController.class);
        serve("/findPlaylists").with(FindPlaylistsController.class);
        serve("/updatePlaylist").with(UpdatePlaylistController.class);
        serve("/choosePlaylists").with(ChoosePlaylistController.class);
        serve("/findTrack").with(FindTrackController.class);
        serve("/login").with(LoginController.class);
        serve("/callback").with(CallbackController.class);

        bind(IBinderDAO.class).to(BinderDAO.class);
        bind(IAddTrackToPlaylistService.class).to(AddTrackToPlaylistService.class);
        bind(IToggleAvailibility.class).to(ToggleAvailibility.class);
        bind(IFindTracksInPlaylistService.class).to(FindTracksInPlaylistService.class);
        bind(IPlaylistDAO.class).to(PlaylistDAO.class);
        bind(IManagePlaylistsService.class).to(ManagePlaylistsService.class);
        bind(ITrackDAO.class).to(TrackDAO.class);
        bind(ISearchTrackService.class).to(SearchTrackService.class);
        bind(ILoginService.class).to(LoginService.class);
        bind(IUserDAO.class).to(UserDAO.class);
    }
}
