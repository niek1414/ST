package oose.dea.Binder.DataSource;

import oose.dea.Track.Domain.Track;

import java.util.List;

public interface IBinderDAO {
    void bind(String playlistId, String trackId, String username);

    List<Track> find(String playlistId, String username);

    void setAvailability(boolean availability, String playlistId);

    boolean getAvailability(String playlistId, String trackId);
}
