package oose.dea.Binder.DataSource;

import com.google.inject.Inject;
import oose.dea.Playlist.Domain.Playlist;
import oose.dea.Track.Domain.Track;
import oose.dea.datasource.DatabaseProperties;
import oose.dea.datasource.SpotifyProperties;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.inject.Singleton;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
@Singleton
public class BinderDAO implements IBinderDAO {
    private static Logger theLogger = Logger.getLogger("PlaylistDAO");

    @Inject
    public BinderDAO() {
    }

    @Override
    public void bind(String playlistId, String trackId, String username) {
        String url = "https://api.spotify.com/v1/users/"+username+"/playlists/"+playlistId+"/tracks";
        String access_token = SpotifyProperties.getAccess_token();

        HttpClient client = new HttpClient();
        PostMethod method = new PostMethod(url);
        method.setRequestHeader("Authorization", "Bearer " + access_token);

        method.setQueryString(new NameValuePair[]{
                new NameValuePair("uris", "spotify:track:"+trackId)
        });

        executePostMethod(client, method);
    }

    @Override
    public List<Track> find(String playlistId, String username) {
        String url = "https://api.spotify.com/v1/users/"+ username +"/playlists/"+ playlistId +"/tracks";
        String access_token = SpotifyProperties.getAccess_token();

        HttpClient client = new HttpClient();
        GetMethod method = new GetMethod(url);
        method.setRequestHeader("Authorization", "Bearer " + access_token);


        byte[] responseBody = executeGetMethod(client, method);

        JSONObject jsonPlaylist = null;
        try {
            jsonPlaylist = (JSONObject) new JSONParser().parse(new String(responseBody));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        JSONArray array = (JSONArray)jsonPlaylist.get("items");
        ArrayList<Track> trackArrayList = new ArrayList<>();

        for (int i = 0; i < array.size(); i++) {
            JSONObject jsonInArray = (JSONObject) array.get(i);
            JSONObject trackInfo = (JSONObject) jsonInArray.get("track");
            JSONObject albumInfo = (JSONObject) trackInfo.get("album");
            JSONArray artists = (JSONArray) trackInfo.get("artists");

            String composedName = "";
            for (int j = 0; j < artists.size(); j++) {
                JSONObject currentArtist = (JSONObject) artists.get(j);
                composedName += currentArtist.get("name").toString();
            }

            String trackName = trackInfo.get("name").toString();
            String id = trackInfo.get("id").toString();
            String uri = trackInfo.get("uri").toString();
            int duration = Integer.parseInt(trackInfo.get("duration_ms").toString())/60;

            Track track = new Track(id, composedName, trackName, uri, duration);

            if (albumInfo.containsKey("images")) {
                JSONArray imageArray = (JSONArray)albumInfo.get("images");
                JSONObject singleImage = (JSONObject)imageArray.get(0);
                String imageURL = String.valueOf(singleImage.get("url"));
                track.setImageURL(imageURL);
            }
            trackArrayList.add(track);
        }
        return trackArrayList;
    }

    @Override
    public void setAvailability(boolean availability, String playlistId) {

    }

    @Override
    public boolean getAvailability(String playlistId, String trackId) {
        return false;
    }

    private byte[] executePostMethod(HttpClient client, PostMethod method) {
        byte[] responseBody = null;

        try {
            int statusCode = client.executeMethod(method);

            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }
            String debugString = new String(method.getResponseBody());
            System.out.println(debugString);
            responseBody = method.getResponseBody();

        } catch (HttpException e) {
            theLogger.severe("Fatal protocol violation: " + e.getMessage());
        } catch (IOException e) {
            theLogger.severe("Fatal transport error: " + e.getMessage());
        } finally {
            method.releaseConnection();
        }
        return responseBody;
    }

    private byte[] executeGetMethod(HttpClient client, GetMethod method) {
        byte[] responseBody = null;

        try {
            int statusCode = client.executeMethod(method);

            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }

            responseBody = method.getResponseBody();

        } catch (HttpException e) {
            theLogger.severe("Fatal protocol violation: " + e.getMessage());
        } catch (IOException e) {
            theLogger.severe("Fatal transport error: " + e.getMessage());
        } finally {
            method.releaseConnection();
        }
        return responseBody;
    }
}
