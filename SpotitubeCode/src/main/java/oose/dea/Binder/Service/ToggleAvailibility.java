package oose.dea.Binder.Service;

import com.google.inject.Inject;
import oose.dea.Binder.DataSource.BinderDAO;
import oose.dea.Binder.DataSource.IBinderDAO;
import oose.dea.Track.Domain.Track;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Singleton
public class ToggleAvailibility implements IToggleAvailibility {
    @Inject
    oose.dea.Binder.DataSource.IBinderDAO IBinderDAO;

    @Override
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void toggleAvailibility(boolean availability, String playlistId){
        IBinderDAO.setAvailability(availability, playlistId);
    }
}
