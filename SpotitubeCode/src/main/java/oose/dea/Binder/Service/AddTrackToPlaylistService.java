package oose.dea.Binder.Service;

import com.google.inject.Inject;
import oose.dea.Binder.DataSource.IBinderDAO;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;
@Singleton
public class AddTrackToPlaylistService implements IAddTrackToPlaylistService {

    @Inject
    IBinderDAO IBinderDAO;

    @Override
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addTrackToPlaylist(String playlistId, String trackId, String username){
        IBinderDAO.bind(playlistId, trackId, username);
    }
}
