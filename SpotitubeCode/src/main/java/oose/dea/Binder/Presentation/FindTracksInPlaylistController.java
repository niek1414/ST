package oose.dea.Binder.Presentation;

import com.google.inject.Inject;
import oose.dea.Binder.Service.FindTracksInPlaylistService;
import oose.dea.Binder.Service.IFindTracksInPlaylistService;
import oose.dea.Binder.Service.IToggleAvailibility;
import oose.dea.Track.Domain.Track;

import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/findTracksInPlaylist")
@Singleton
public class FindTracksInPlaylistController extends HttpServlet {
    @Inject
    private IFindTracksInPlaylistService IFindTracksInPlaylistService;
    @Inject
    private IToggleAvailibility IToggleAvailibility;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String playlistId = request.getParameter("playlist");
        String ownerId = request.getParameter("owner");
        List<Track> trackListrack = IFindTracksInPlaylistService.findTrackInPlaylist(playlistId, ownerId);
        request.setAttribute("all", trackListrack);
        request.setAttribute("playlist", playlistId);
        request.getRequestDispatcher("findTracksInPlaylist.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String playlistId = request.getParameter("playlist");
        String ownerId = request.getParameter("owner");
        boolean availability = Boolean.parseBoolean((request.getParameter("availability")));
        IToggleAvailibility.toggleAvailibility(!availability,playlistId);
        List<Track> trackListrack = IFindTracksInPlaylistService.findTrackInPlaylist(playlistId, ownerId);
        request.setAttribute("all", trackListrack);
        request.setAttribute("playlist", playlistId);
        request.getRequestDispatcher("findTracksInPlaylist.jsp").forward(request, response);
    }
}
