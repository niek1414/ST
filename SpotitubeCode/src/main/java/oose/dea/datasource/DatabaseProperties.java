package oose.dea.datasource;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Logger;

public class DatabaseProperties {

    private Properties prop = new Properties();
    private String driver;
    private String connectionstring;
    private static Logger theLogger = Logger.getLogger("DatabaseProperties");

    public DatabaseProperties () {
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            prop.load(classLoader.getResourceAsStream("database.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        driver = prop.getProperty("driver");
        connectionstring = prop.getProperty("connectionstring");
    }

    public Connection OpenConnection () {
        try {
            Connection connection = DriverManager.getConnection(this.getConnectionstring());
            return connection;
        } catch (SQLException e) {
            theLogger.finest("Connection failed");
        }
        return null;
    }

    public String getDriver () {
        return driver;
    }

    public String getConnectionstring() {
        return connectionstring;
    }
}
