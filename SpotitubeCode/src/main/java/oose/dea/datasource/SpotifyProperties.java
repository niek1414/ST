package oose.dea.datasource;

public class SpotifyProperties {
    private static String access_token = "";

    public static String getAccess_token() {
        return access_token;
    }

    public static void setAccess_token(String access_token) {
        SpotifyProperties.access_token = access_token;
    }
}
