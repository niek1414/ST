package oose.dea.Playlist.Service;

import com.google.inject.Inject;
import oose.dea.Playlist.DataSource.IPlaylistDAO;
import oose.dea.Playlist.Domain.Playlist;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/playlist")
public class ManagePlaylistsService implements IManagePlaylistsService {

    @Inject
    IPlaylistDAO IPlaylistDAO;

    @Inject
    public ManagePlaylistsService(IPlaylistDAO playlistDAO) {
        IPlaylistDAO = playlistDAO;
    }

    @Override
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addPlaylist(Playlist playlist, String username){
        IPlaylistDAO.save(playlist,username);
    }

    @Override
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Playlist> getPlaylists() {
        return IPlaylistDAO.list();
    }

    @Override
    @GET
    @Path("/{owner}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Playlist> getPlaylistsByOwner(@PathParam("owner") final String owner) {
        return IPlaylistDAO.findByOwner(owner);
    }

    @Override
    @DELETE
    @Path("/{playlistId}/{username}")
    public void deletePlaylist(@PathParam("playlistId") final String playlistId, @PathParam("username") String username)
    {
        IPlaylistDAO.delete(playlistId, username);
    }

    @Override
    @PUT
    @Path("/{playlistId}/{username}/{newName}")
    public void updatePlaylist(@PathParam("playlistId") final String playlistId, @PathParam("username") String username, @PathParam("newName") String newName){
        IPlaylistDAO.edit(playlistId, username, newName);
    }
}
