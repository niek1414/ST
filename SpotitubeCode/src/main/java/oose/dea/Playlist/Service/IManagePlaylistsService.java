package oose.dea.Playlist.Service;

import oose.dea.Playlist.Domain.Playlist;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

public interface IManagePlaylistsService {
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    void addPlaylist(Playlist playlist, String username);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<Playlist> getPlaylists();

    @GET
    @Path("/{owner}")
    @Produces(MediaType.APPLICATION_JSON)
    List<Playlist> getPlaylistsByOwner(@PathParam("owner") String owner);

    @DELETE
    @Path("/{playlistId}/{username}")
    void deletePlaylist(@PathParam("playlistId") String playlistId, @PathParam("username") String username);

    @PUT
    @Path("/{playlistId}/{username}/{newName}")
    void updatePlaylist(@PathParam("playlistId") String playlistId, @PathParam("username") String username, @PathParam("newName") String newName);
}
