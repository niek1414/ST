package oose.dea.Playlist.DataSource;

import oose.dea.Playlist.Domain.Playlist;

import java.util.List;

public interface IPlaylistDAO {
    List<Playlist> list();

    List<Playlist> findByOwner(String owner);

    void save(Playlist playlist, String username);

    void delete(String playlistId, String username);

    void edit(String playlistId, String name, String newName);
}
