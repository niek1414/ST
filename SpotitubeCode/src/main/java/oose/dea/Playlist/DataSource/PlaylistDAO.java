package oose.dea.Playlist.DataSource;

import com.google.inject.Inject;
import oose.dea.Playlist.Domain.Playlist;

import javax.inject.Singleton;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import oose.dea.datasource.SpotifyProperties;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@Singleton
public class PlaylistDAO implements IPlaylistDAO {
    private static Logger theLogger = Logger.getLogger("PlaylistDAO");

    @Inject
    public PlaylistDAO() {

    }


    @Override
    public List<Playlist> list() {
        return null;
        //Returning all lists of Spotify is impassable, just to many..
    }

    @Override
    public List<Playlist> findByOwner(String username) {
        String url = "https://api.spotify.com/v1/users/" + username + "/playlists";
        String access_token = SpotifyProperties.getAccess_token();

        HttpClient client = new HttpClient();
        GetMethod method = new GetMethod(url);
        method.setRequestHeader("Authorization", "Bearer " + access_token);


        byte[] responseBody = executeGetMethod(client, method);

        JSONObject jsonPlaylist = null;
        try {
            jsonPlaylist = (JSONObject) new JSONParser().parse(new String(responseBody));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        JSONArray array = (JSONArray)jsonPlaylist.get("items");
        ArrayList<Playlist> playlistArrayList = new ArrayList<>();

        for (int i = 0; i < array.size(); i++) {
            JSONObject jsonInArray = (JSONObject) array.get(i);
            JSONObject ownerInfo = (JSONObject) jsonInArray.get("owner");

            String playlistName = jsonInArray.get("name").toString();
            String ownerName = ownerInfo.get("id").toString();

            String id = jsonInArray.get("id").toString();

            Playlist playlist = new Playlist(id, ownerName, playlistName);
            if (jsonInArray.containsKey("images")) {
                JSONArray imageArray = (JSONArray)jsonInArray.get("images");
                if (imageArray.size() > 0) {
                    JSONObject singleImage = (JSONObject) imageArray.get(0);
                    String imageURL = String.valueOf(singleImage.get("url"));
                    playlist.setImageURL(imageURL);
                }
            }

            playlistArrayList.add(playlist);
        }
        return playlistArrayList;
    }

    @Override
    public void save(Playlist playlist, String username) {
        String url = "https://api.spotify.com/v1/users/" + username + "/playlists";
        String access_token = SpotifyProperties.getAccess_token();

        String playlistName = playlist.getName();

        HttpClient client = new HttpClient();
        PostMethod method = new PostMethod(url);
        method.setRequestHeader("Authorization", "Bearer " + access_token);

        JSONObject jsonString = new JSONObject();
        jsonString.put("name", playlistName);
        jsonString.put("public",false);

        StringRequestEntity requestEntity = null;
        try {
            requestEntity = new StringRequestEntity(
                    jsonString.toJSONString(),
                    "application/json",
                    "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        method.setRequestEntity(requestEntity);

        executePostMethod(client, method);

    }

    @Override
    public void delete(String playlistId, String username) {
        String url = "https://api.spotify.com/v1/users/" + username + "/playlists/" + playlistId + "/followers";
        String access_token = SpotifyProperties.getAccess_token();

        HttpClient client = new HttpClient();
        DeleteMethod method = new DeleteMethod(url);
        method.setRequestHeader("Authorization", "Bearer " + access_token);

        executeDeleteMethod(client, method);
    }

    @Override
    public void edit(String playlistId, String username, String newName) {
        String url = "https://api.spotify.com/v1/users/" + username + "/playlists/" + playlistId;
        String access_token = SpotifyProperties.getAccess_token();

        HttpClient client = new HttpClient();
        PutMethod method = new PutMethod(url);
        method.setRequestHeader("Authorization", "Bearer " + access_token);

        JSONObject jsonString = new JSONObject();
        jsonString.put("name", newName);
        jsonString.put("public",false);

        StringRequestEntity requestEntity = null;
        try {
            requestEntity = new StringRequestEntity(
                    jsonString.toJSONString(),
                    "application/json",
                    "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        method.setRequestEntity(requestEntity);

        executePutMethod(client, method);
    }

    private byte[] executeGetMethod(HttpClient client, GetMethod method) {
        byte[] responseBody = null;

        try {
            int statusCode = client.executeMethod(method);

            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }

            responseBody = method.getResponseBody();

        } catch (HttpException e) {
            theLogger.severe("Fatal protocol violation: " + e.getMessage());
        } catch (IOException e) {
            theLogger.severe("Fatal transport error: " + e.getMessage());
        } finally {
            method.releaseConnection();
        }
        return responseBody;
    }


    private byte[] executePostMethod(HttpClient client, PostMethod method) {
        byte[] responseBody = null;

        try {
            int statusCode = client.executeMethod(method);

            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }

            responseBody = method.getResponseBody();
        } catch (HttpException e) {
            theLogger.severe("Fatal protocol violation: " + e.getMessage());
        } catch (IOException e) {
            theLogger.severe("Fatal transport error: " + e.getMessage());
        } finally {
            method.releaseConnection();
        }
        return responseBody;
    }

    private void executeDeleteMethod(HttpClient client, DeleteMethod method) {
        try {
            int statusCode = client.executeMethod(method);

            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }

        } catch (HttpException e) {
            theLogger.severe("Fatal protocol violation: " + e.getMessage());
        } catch (IOException e) {
            theLogger.severe("Fatal transport error: " + e.getMessage());
        } finally {
            method.releaseConnection();
        }
    }

    private void executePutMethod(HttpClient client, PutMethod method) {
        try {
            int statusCode = client.executeMethod(method);

            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }

        } catch (HttpException e) {
            theLogger.severe("Fatal protocol violation: " + e.getMessage());
        } catch (IOException e) {
            theLogger.severe("Fatal transport error: " + e.getMessage());
        } finally {
            method.releaseConnection();
        }
    }
}
