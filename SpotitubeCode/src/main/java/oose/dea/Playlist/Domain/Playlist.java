package oose.dea.Playlist.Domain;

public class Playlist {
    private String idPlaylist;
    private String owner;
    private String name;
    private String imageURL;

    private String defaultImageURL = "https://d3rt1990lpmkn.cloudfront.net/640/907e87639091f8805c48681d9e7f144dedf53741";

    public Playlist(String idPlaylist, String owner, String name) {
        this.idPlaylist = idPlaylist;
        this.owner = owner;
        this.name = name;
        this.imageURL = defaultImageURL;
    }

    public String getOwner() {
        return owner;
    }

    public String getName() {
        return name;
    }

    public String getIdPlaylist() {
        return idPlaylist;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
