package oose.dea.Playlist.Presentation;

import com.google.inject.Inject;
import oose.dea.Playlist.Domain.Playlist;
import oose.dea.Playlist.Service.IManagePlaylistsService;

import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@Singleton
@WebServlet(urlPatterns = "/addPlaylist")
public class AddPlaylistController extends HttpServlet {
    @Inject
    private IManagePlaylistsService IManagePlaylistsService;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Playlist playlist = new Playlist("0", request.getSession().getAttribute("naam").toString(), request.getParameter("playlistnaam").toString());
        IManagePlaylistsService.addPlaylist(playlist, String.valueOf(request.getSession().getAttribute("naam")));
        request.getRequestDispatcher("addPlaylist.jsp").forward(request, response);
    }
}