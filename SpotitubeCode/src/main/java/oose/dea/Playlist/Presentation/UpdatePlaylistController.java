package oose.dea.Playlist.Presentation;

import com.google.inject.Inject;
import oose.dea.Playlist.Service.IManagePlaylistsService;
import oose.dea.Playlist.Service.ManagePlaylistsService;

import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/updatePlaylist")
@Singleton
public class UpdatePlaylistController extends HttpServlet {
    @Inject
    private IManagePlaylistsService IManagePlaylistsService;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String playlistid = request.getParameter("playlist");
        String name = request.getParameter("name");
        IManagePlaylistsService.updatePlaylist(playlistid, String.valueOf(request.getSession().getAttribute("naam")), name);
        request.getRequestDispatcher("updatePlaylist.jsp").forward(request, response);
    }
}