package oose.dea.Track.DataSource;

import com.google.inject.Inject;
import oose.dea.Track.Domain.Track;
import oose.dea.datasource.DatabaseProperties;
import oose.dea.datasource.SpotifyProperties;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.inject.Singleton;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
@Singleton
public class TrackDAO implements ITrackDAO {
    private static Logger theLogger = Logger.getLogger("TrackDAO");

    @Inject
    public TrackDAO() {
    }

    @Override
    public List<Track> list() {
        return null;
        //Returning all tracks of Spotify is impassable, just to many..
    }

    @Override
    public List<Track> findByTitle(String title) {
        String url = "https://api.spotify.com/v1/search";
        String access_token = SpotifyProperties.getAccess_token();

        HttpClient client = new HttpClient();
        GetMethod method = new GetMethod(url);
        method.setRequestHeader("Authorization", "Bearer " + access_token);

        method.setQueryString(new NameValuePair[]{
                new NameValuePair("q", title),
                new NameValuePair("type", "track"),
                new NameValuePair("limit", "50")
        });

        byte[] responseBody = executeGetMethod(client, method);

        JSONObject jsonPlaylist = null;
        try {
            jsonPlaylist = (JSONObject) ((JSONObject) new JSONParser().parse(new String(responseBody))).get("tracks");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        JSONArray array = (JSONArray)jsonPlaylist.get("items");
        ArrayList<Track> trackArrayList = new ArrayList<>();

        for (int i = 0; i < array.size(); i++) {
            JSONObject jsonInArray = (JSONObject) array.get(i);
            JSONObject albumInfo = (JSONObject) jsonInArray.get("album");
            JSONArray artists = (JSONArray) jsonInArray.get("artists");

            String composedName = "";
            for (int j = 0; j < artists.size(); j++) {
                JSONObject currentArtist = (JSONObject) artists.get(j);
                composedName += currentArtist.get("name").toString();
            }

            String trackName = jsonInArray.get("name").toString();
            String id = jsonInArray.get("id").toString();
            String uri = jsonInArray.get("uri").toString();
            int duration = Integer.parseInt(jsonInArray.get("duration_ms").toString())/60;

            Track track = new Track(id, composedName, trackName, uri, duration);

            if (albumInfo.containsKey("images")) {
                JSONArray imageArray = (JSONArray)albumInfo.get("images");
                if (imageArray.size() > 0) {
                    JSONObject singleImage = (JSONObject) imageArray.get(0);
                    String imageURL = String.valueOf(singleImage.get("url"));
                    track.setImageURL(imageURL);
                }
            }
            trackArrayList.add(track);
        }
        return trackArrayList;
    }

    private byte[] executeGetMethod(HttpClient client, GetMethod method) {
        byte[] responseBody = null;

        try {
            int statusCode = client.executeMethod(method);

            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }

            responseBody = method.getResponseBody();

        } catch (HttpException e) {
            theLogger.severe("Fatal protocol violation: " + e.getMessage());
        } catch (IOException e) {
            theLogger.severe("Fatal transport error: " + e.getMessage());
        } finally {
            method.releaseConnection();
        }
        return responseBody;
    }
}
