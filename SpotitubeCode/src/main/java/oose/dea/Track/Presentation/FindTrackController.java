package oose.dea.Track.Presentation;

import com.google.inject.Inject;
import oose.dea.Track.Domain.Track;
import oose.dea.Track.Service.ISearchTrackService;

import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/findTrack")
@Singleton
public class FindTrackController extends HttpServlet {
    @Inject
    private ISearchTrackService ISearchTrackService;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Track> track = ISearchTrackService.getTracks();

        request.setAttribute("all", track);
        request.getRequestDispatcher("findTrack.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Track> track;
        String search = request.getParameter("search");
        track = ISearchTrackService.getTracksByTitle(search);

        request.setAttribute("search", search);
        request.setAttribute("all", track);
        request.getRequestDispatcher("findTrack.jsp").forward(request, response);
    }
}
