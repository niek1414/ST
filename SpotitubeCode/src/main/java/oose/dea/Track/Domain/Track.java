package oose.dea.Track.Domain;

public class Track {
    private String idTrack;
    private String performer;
    private String title;
    private String url;
    private int duration;
    private boolean availability;
    private String imageURL;

    private String defaultImageURL = "https://d3rt1990lpmkn.cloudfront.net/640/907e87639091f8805c48681d9e7f144dedf53741";

    public Track(String idTrack, String performer, String title, String url, int duration) {
        this.idTrack = idTrack;
        this.performer = performer;
        this.title = title;
        this.url = url;
        this.duration = duration;
        this.availability = false;
        this.imageURL = defaultImageURL;
    }

    public String getIdTrack() {
        return idTrack;
    }

    public String getPerformer() {
        return performer;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public int getDuration() {
        return duration;
    }

    public boolean getAvailability() { return availability; }

    public void setAvailability(boolean availability) { this.availability = availability; }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
