package oose.dea.User.DataSource;

import oose.dea.User.Domain.User;
import org.apache.commons.httpclient.URIException;

/**
 * Created by NK on 1-11-2015.
 */
public interface IUserDAO {
    String getAccessToken(String code);

    String getAuthorisationURL();

    User getCurrentUser();
}
