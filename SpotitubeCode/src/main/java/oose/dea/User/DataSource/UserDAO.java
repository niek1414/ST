package oose.dea.User.DataSource;

import oose.dea.User.Domain.User;
import oose.dea.datasource.SpotifyProperties;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.inject.Singleton;
import java.io.IOException;
import java.util.logging.Logger;

@Singleton
public class UserDAO implements IUserDAO {

    private static Logger theLogger = Logger.getLogger("userDAO");

    String redirect_uri = "http://localhost:8080/callback";
    String client_id = "0072040097254e4d803241d5d94849c7";
    String client_secret = "1f68dbd986ac44aead45480cdd9fc085";

    @Override
    public String getAccessToken(String code) {
        String url = "https://accounts.spotify.com/api/token";

        HttpClient client = new HttpClient();
        PostMethod method = new PostMethod(url);

        method.setRequestBody
                (new NameValuePair[]{
                        new NameValuePair("grant_type", "authorization_code"),
                        new NameValuePair("code", code),
                        new NameValuePair("redirect_uri", redirect_uri)
                });
        method.setParameter("client_id", client_id);
        method.setParameter("client_secret", client_secret);

        byte[] responseBody = executePostMethod(client, method);
        String accessToken = null;

        try {
            JSONObject json = (JSONObject) new JSONParser().parse(new String(responseBody));
            accessToken = json.get("access_token").toString();

        } catch (ParseException e) {
            theLogger.severe("Error: " + e.getMessage());
        }
        return accessToken;
    }

    @Override
    public String getAuthorisationURL() {
        String url = "https://accounts.spotify.com/authorize";

        HttpClient client = new HttpClient();
        GetMethod method = new GetMethod(url);

        method.setQueryString(new NameValuePair[]{
                new NameValuePair("client_id", client_id),
                new NameValuePair("response_type", "code"),
                new NameValuePair("redirect_uri", redirect_uri),
                new NameValuePair("scope", "playlist-read-private " +
                        "playlist-read-collaborative " +
                        "playlist-modify-public " +
                        "playlist-modify-private " +
                        "user-follow-modify " +
                        "user-follow-read " +
                        "user-library-read " +
                        "user-library-modify " +
                        "user-read-private " +
                        "user-read-birthdate " +
                        "user-read-email")
        });

        String finalURL = null;
        try {
            finalURL = method.getURI().toString();
        } catch (URIException e) {
            e.printStackTrace();
        }
        return finalURL;
    }

    @Override
    public User getCurrentUser(){
        String url = "https://api.spotify.com/v1/me";

        String access_token = SpotifyProperties.getAccess_token();

        HttpClient client = new HttpClient();
        GetMethod method = new GetMethod(url);
        method.setRequestHeader("Authorization", "Bearer " + access_token);

        byte[] responseBody = executeGetMethod(client,method);

        JSONObject json = null;
        try {
            json = (JSONObject) new JSONParser().parse(new String(responseBody));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        JSONObject urls = (JSONObject) json.get("external_urls");

        User user = new User(json.get("id").toString(),urls.get("spotify").toString());
        return user;
    }

    private byte[] executePostMethod(HttpClient client, PostMethod method) {
        byte[] responseBody = null;

        try {
            int statusCode = client.executeMethod(method);

            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }

            responseBody = method.getResponseBody();

        } catch (HttpException e) {
            theLogger.severe("Fatal protocol violation: " + e.getMessage());
        } catch (IOException e) {
            theLogger.severe("Fatal transport error: " + e.getMessage());
        } finally {
            method.releaseConnection();
        }
        return responseBody;
    }

    private byte[] executeGetMethod(HttpClient client, GetMethod method) {
        byte[] responseBody = null;

        try {
            int statusCode = client.executeMethod(method);

            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }

            responseBody = method.getResponseBody();

        } catch (HttpException e) {
            theLogger.severe("Fatal protocol violation: " + e.getMessage());
        } catch (IOException e) {
            theLogger.severe("Fatal transport error: " + e.getMessage());
        } finally {
            method.releaseConnection();
        }
        return responseBody;
    }
}
