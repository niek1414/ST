package oose.dea.User.Presentation;

import com.google.inject.Inject;
import oose.dea.User.Service.ILoginService;

import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/login")
@Singleton
public class LoginController extends HttpServlet{

    @Inject
    private ILoginService loginService;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String finishedURL = loginService.getAuthorisationURL();
        request.setAttribute("page", finishedURL);
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }
}

