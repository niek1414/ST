package oose.dea.User.Presentation;

import com.google.inject.Inject;
import oose.dea.User.Domain.User;
import oose.dea.User.Service.ILoginService;

import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/callback")
@Singleton
public class CallbackController extends HttpServlet {
    String code;
    @Inject
    private ILoginService loginService;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String string = request.getQueryString();
        String[] splitArray = string.split("=");
        for (int i = 0; i < splitArray.length; i++) {
            switch (splitArray[i]){
                case "code":{
                    i++;
                    code = splitArray[i];
                }break;
                default:{
                    i+=2;
                }
            }
        }

        String access_token = loginService.getAccessToken(code);
        loginService.setAccess_token(access_token);
        User user = loginService.getCurrentUser();

        request.getSession().setAttribute("naam", user.getUsername());
        request.getSession().setAttribute("access_token", access_token);
        request.getRequestDispatcher("menu.jsp").forward(request, response);
    }

}
