package oose.dea.User.Service;

import com.google.inject.Inject;
import oose.dea.User.DataSource.IUserDAO;
import oose.dea.User.Domain.User;
import oose.dea.datasource.SpotifyProperties;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/login")
public class LoginService implements ILoginService {

    @Inject
    IUserDAO userDAO;

    @Inject
    public LoginService() {
    }

    @Override
    @GET
    @Path("/{code}")
    @Consumes(MediaType.APPLICATION_JSON)
    public String getAccessToken(@PathParam("code") String code){
        return userDAO.getAccessToken(code);
    }

    @Override
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getAuthorisationURL() {
        return userDAO.getAuthorisationURL();
    }

    @Override
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public User getCurrentUser() {
        return userDAO.getCurrentUser();
    }

    @Override
    @PUT
    @Path("/{access_token}")
    @Produces(MediaType.APPLICATION_JSON)
    public void setAccess_token(@PathParam("access_token") String access_token) {
        SpotifyProperties.setAccess_token(access_token);
    }
}
