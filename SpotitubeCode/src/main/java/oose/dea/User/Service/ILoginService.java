package oose.dea.User.Service;

import oose.dea.User.Domain.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

public interface ILoginService {
    @GET
    @Path("/{code}")
    @Consumes(MediaType.APPLICATION_JSON)
    String getAccessToken(@PathParam("code") String code);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    String getAuthorisationURL();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    User getCurrentUser();

    @PUT
    @Path("/{access_token}")
    @Produces(MediaType.APPLICATION_JSON)
    void setAccess_token(@PathParam("access_token") String access_token);
}
