<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login complete</title>
</head>
<body>
    <form method="post" action="/showMyPlaylists">
        <input type="submit" value="Show my playlists">
        <input type="hidden" value="<%=request.getAttribute("access_token")%>" name="access_token">
    </form>
    <p> access token: <%=request.getAttribute("access_token")%> </p>
</body>
</html>
