<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
    <h1>Alle playlists van:</h1>
    <p> Gebruikersnaam: <%=request.getAttribute("userName")%> </p>
    <p> URL: <%=request.getAttribute("url")%> </p>
    <hr>
    <table>
        <thead>
        <th>Naam</th>
        <th>Track totaal</th>
        <th>Link</th>
        <th>Cover</th>
        </thead>
        <tbody>
            <c:forEach items="${playlistList}" var="current">
                <tr>
                    <td>
                        <c:out value="${current.name}"/>
                    </td>
                    <td>
                        <c:out value="${current.trackTotal}"/>
                    </td>
                    <td>
                        <a href="<c:out value="${current.applicationURL}"/>">[Naar album in spotify]</a>
                    </td>
                    <td>
                        <img src="<c:out value="${current.urlImage}"/>" alt="" width="150" height="150">
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</body>
</html>
