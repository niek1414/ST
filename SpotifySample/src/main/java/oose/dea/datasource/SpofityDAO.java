package oose.dea.datasource;

import oose.dea.Domein.Playlist;
import oose.dea.Domein.User;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;

public class SpofityDAO {

    String redirect_uri = "http://localhost:8080/callback";
    String client_id = "0072040097254e4d803241d5d94849c7";
    String client_secret = "1f68dbd986ac44aead45480cdd9fc085";

    private byte[] executeGetMethod(HttpClient client, GetMethod method) {
        byte[] responseBody = null;

        try {
            int statusCode = client.executeMethod(method);

            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }

            responseBody = method.getResponseBody();

        } catch (HttpException e) {
            System.err.println("Fatal protocol violation: " + e.getMessage());
            e.printStackTrace();
            //USE LOGGER IF CREATED
        } catch (IOException e) {
            System.err.println("Fatal transport error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            method.releaseConnection();
        }
        return responseBody;
    }

    private byte[] executePostMethod(HttpClient client, PostMethod method) {
        byte[] responseBody = null;

        try {
            int statusCode = client.executeMethod(method);

            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }

            responseBody = method.getResponseBody();

        } catch (HttpException e) {
            System.err.println("Fatal protocol violation: " + e.getMessage());
            e.printStackTrace();
            //USE LOGGER IF CREATED
        } catch (IOException e) {
            System.err.println("Fatal transport error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            method.releaseConnection();
        }
        return responseBody;
    }

    public String getAccessToken(String code) {
        String url = "https://accounts.spotify.com/api/token";

        HttpClient client = new HttpClient();
        PostMethod method = new PostMethod(url);

        method.setRequestBody
                (new NameValuePair[]{
                        new NameValuePair("grant_type", "authorization_code"),
                        new NameValuePair("code", code),
                        new NameValuePair("redirect_uri", redirect_uri)
                });
        method.setParameter("client_id", client_id);
        method.setParameter("client_secret", client_secret);

        byte[] responseBody = executePostMethod(client, method);
        String accessToken = null;

        try {
            JSONObject json = (JSONObject) new JSONParser().parse(new String(responseBody));
            accessToken = json.get("access_token").toString();

        } catch (ParseException e) {
            //USE LOGGER IF CREATED
            e.printStackTrace();
        }
        return accessToken;
    }

    public User getCurrentUser(String access_token){
        String url = "https://api.spotify.com/v1/me";

        HttpClient client = new HttpClient();
        GetMethod method = new GetMethod(url);
        method.setRequestHeader("Authorization", "Bearer " + access_token);

        byte[] responseBody = executeGetMethod(client,method);

        JSONObject json = null;
        try {
            json = (JSONObject) new JSONParser().parse(new String(responseBody));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        JSONObject urls = (JSONObject) json.get("external_urls");

        User user = new User(json.get("id").toString(),urls.get("spotify").toString());
        return user;
    }

    public ArrayList<Playlist> getPlaylistsOfUser(String username, String access_token) {
        String url = "https://api.spotify.com/v1/users/" + username + "/playlists";

        HttpClient client = new HttpClient();
        GetMethod method = new GetMethod(url);
        method.setRequestHeader("Authorization", "Bearer " + access_token);


        byte[] responseBody = executeGetMethod(client, method);

        JSONObject jsonPlaylist = null;
        try {
            jsonPlaylist = (JSONObject) new JSONParser().parse(new String(responseBody));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        JSONArray array = (JSONArray)jsonPlaylist.get("items");
        ArrayList<Playlist> playlistArrayList = new ArrayList<>();

        for (int i = 0; i < array.size(); i++) {
            JSONObject jsonInArray = (JSONObject) array.get(i);

            String playlistName = jsonInArray.get("name").toString();

            JSONObject tracks = (JSONObject) jsonInArray.get("tracks");
            int trackTotal = Integer.parseInt(tracks.get("total").toString());

            String applicationURL = jsonInArray.get("uri").toString();

            Playlist playlist = new Playlist(playlistName, trackTotal, applicationURL);
            if (jsonInArray.containsKey("images")) {
                JSONArray imageArray = (JSONArray)jsonInArray.get("images");
                JSONObject singleImage = (JSONObject)imageArray.get(0);
                String imageURL = String.valueOf(singleImage.get("url"));
                playlist.setUrlImage(imageURL);
            }

            playlistArrayList.add(playlist);
        }
        return playlistArrayList;
    }

    public String getAuthorisationURL() throws URIException {
        String url = "https://accounts.spotify.com/authorize";

        HttpClient client = new HttpClient();
        GetMethod method = new GetMethod(url);

        method.setQueryString(new NameValuePair[]{
                new NameValuePair("client_id", client_id),
                new NameValuePair("response_type", "code"),
                new NameValuePair("redirect_uri", redirect_uri),
                new NameValuePair("scope", "playlist-read-private " +
                        "playlist-read-collaborative " +
                        "playlist-modify-public " +
                        "playlist-modify-private " +
                        "user-follow-modify " +
                        "user-follow-read " +
                        "user-library-read " +
                        "user-library-modify " +
                        "user-read-private " +
                        "user-read-birthdate " +
                        "user-read-email")
        });

        return method.getURI().toString();
    }
}
