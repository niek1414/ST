package oose.dea.datasource;

import org.glassfish.jersey.server.ResourceConfig;
import javax.ws.rs.ApplicationPath;

@ApplicationPath("/rest")
public class RestConfig extends ResourceConfig {

    private static final String JSON_SERIALIZER = "jersey.config.server.provider.packages";
    private static final String JACKSON_JSON_SERIALIZE = "com.fasterxml.jackson.jaxrs.json;service";

    public RestConfig() {
        packages(true, "oose.dea.datasource");
        property(JSON_SERIALIZER,JACKSON_JSON_SERIALIZE);
    }
}
