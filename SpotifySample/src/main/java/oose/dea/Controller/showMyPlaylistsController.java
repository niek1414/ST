package oose.dea.Controller;

import oose.dea.Domein.Playlist;
import oose.dea.Domein.User;
import oose.dea.datasource.SpofityDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;


@WebServlet(urlPatterns = "/showMyPlaylists")
public class ShowMyPlaylistsController extends HttpServlet {

    //USE GUICE IF AVAILABLE
    SpofityDAO spofityDAO = new SpofityDAO();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String access_token = request.getParameter("access_token");

        User user = spofityDAO.getCurrentUser(access_token);

        ArrayList<Playlist> playlistArrayList = spofityDAO.getPlaylistsOfUser(user.getUsername(),access_token);

        request.setAttribute("userName", user.getUsername());
        request.setAttribute("url", user.getUrl());

        request.setAttribute("playlistList", playlistArrayList);

        request.getRequestDispatcher("showMyPlaylists.jsp").forward(request, response);
    }

}
