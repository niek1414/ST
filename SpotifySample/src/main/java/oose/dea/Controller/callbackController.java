package oose.dea.Controller;

import oose.dea.datasource.SpofityDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/callback")
public class CallbackController extends HttpServlet {
    String code;

    //USE GUICE IF AVAILABLE
    SpofityDAO spofityDAO = new SpofityDAO();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String string = request.getQueryString();
        String[] splitArray = string.split("=");
        for (int i = 0; i < splitArray.length; i++) {
            switch (splitArray[i]){
                case "code":{
                    i++;
                    code = splitArray[i];
                }break;
                default:{
                    i+=2;
                }
            }
        }

        String access_token = spofityDAO.getAccessToken(code);

        request.setAttribute("code", code);
        request.setAttribute("access_token", access_token);
        request.getRequestDispatcher("callback.jsp").forward(request, response);
    }

}
