package oose.dea.Controller;

import oose.dea.datasource.SpofityDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/login")
public class LoginController extends HttpServlet{

    //USE GUICE IF AVAILABLE
    SpofityDAO spofityDAO = new SpofityDAO();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String finishedURL = spofityDAO.getAuthorisationURL();
        request.setAttribute("page", finishedURL);
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }
}

