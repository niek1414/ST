package oose.dea.Domein;

public class Playlist {
    public String name;
    public String urlImage = "https://developer.spotify.com/wp-content/themes/sp-wp-theme/images/logo-navbar.png";//DEFAULT IF MISSING
    public int trackTotal;
    public String applicationURL;

    public Playlist(String name, int trackTotal, String applicationURL){
        this.name = name;
        this.trackTotal = trackTotal;
        this.applicationURL = applicationURL;
    }

    public void setUrlImage(String urlImage){
        this.urlImage = urlImage;
    }

    public String getName() {
        return name;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public int getTrackTotal() {
        return trackTotal;
    }

    public String getApplicationURL() {
        return applicationURL;
    }
}
