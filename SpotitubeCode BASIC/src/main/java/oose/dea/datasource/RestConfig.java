package oose.dea.datasource;

import com.google.inject.Guice;
import com.google.inject.Inject;
import oose.dea.Guice.AppBinding;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.server.ResourceConfig;
import org.jvnet.hk2.guice.bridge.api.GuiceBridge;
import org.jvnet.hk2.guice.bridge.api.GuiceIntoHK2Bridge;

import javax.inject.Singleton;
import javax.ws.rs.ApplicationPath;

@ApplicationPath("/rest")
public class RestConfig extends ResourceConfig {

    private static final String JSON_SERIALIZER = "jersey.config.server.provider.packages";
    private static final String JACKSON_JSON_SERIALIZE = "com.fasterxml.jackson.jaxrs.json;service";

    @Inject
    public RestConfig(ServiceLocator serviceLocator) {
        packages(true, "oose.dea.datasource");
        property(JSON_SERIALIZER,JACKSON_JSON_SERIALIZE);
        GuiceBridge.getGuiceBridge().initializeGuiceBridge(serviceLocator);
        GuiceIntoHK2Bridge guiceBridge = serviceLocator.getService(GuiceIntoHK2Bridge.class);
        guiceBridge.bridgeGuiceInjector(Guice.createInjector(new AppBinding()));
    }
}
