package oose.dea.Binder.Service;

import oose.dea.Track.Domain.Track;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;
import java.util.List;

public interface IFindTracksInPlaylistService {
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    List<Track> findTrackInPlaylist(int playlistId);
}
