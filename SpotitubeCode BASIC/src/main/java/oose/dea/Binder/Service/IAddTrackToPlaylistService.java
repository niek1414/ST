package oose.dea.Binder.Service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;

public interface IAddTrackToPlaylistService {
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    void addTrackToPlaylist(int playlistId, int trackId);
}
