package oose.dea.Binder.Presentation;

import com.google.inject.Inject;
import oose.dea.Binder.Service.FindTracksInPlaylistService;
import oose.dea.Binder.Service.IFindTracksInPlaylistService;
import oose.dea.Track.Domain.Track;

import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/findTracksInPlaylist")
@Singleton
public class FindTracksInPlaylistController extends HttpServlet {
    @Inject
    private IFindTracksInPlaylistService IFindTracksInPlaylistService;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int playlistId = Integer.parseInt(request.getParameter("playlist"));
        List<Track> trackListrack = IFindTracksInPlaylistService.findTrackInPlaylist(playlistId);
        request.setAttribute("all", trackListrack);
        request.getRequestDispatcher("findTracksInPlaylist.jsp").forward(request, response);
    }
}
