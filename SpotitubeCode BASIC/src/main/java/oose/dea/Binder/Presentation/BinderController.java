package oose.dea.Binder.Presentation;

import com.google.inject.Inject;
import oose.dea.Binder.Service.AddTrackToPlaylistService;
import oose.dea.Binder.Service.IAddTrackToPlaylistService;

import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.annotation.Annotation;

@WebServlet(urlPatterns = "/binder")
@Singleton
public class BinderController extends HttpServlet  {
    @Inject
    private IAddTrackToPlaylistService IAddTrackToPlaylistService;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int playlistid = Integer.parseInt(request.getParameter("playlist"));
        String s = request.getParameter("track");
        int trackid = Integer.parseInt(s);

        IAddTrackToPlaylistService.addTrackToPlaylist(playlistid, trackid);
        request.getRequestDispatcher("binder.jsp").forward(request, response);
    }
}
