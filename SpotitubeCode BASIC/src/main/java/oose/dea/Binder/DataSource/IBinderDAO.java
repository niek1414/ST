package oose.dea.Binder.DataSource;

import oose.dea.Track.Domain.Track;

import java.util.List;

public interface IBinderDAO {
    void bind(int playlistId, int trackId);

    List<Track> find(int playlistId);
}
