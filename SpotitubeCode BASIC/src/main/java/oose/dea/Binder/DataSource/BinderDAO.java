package oose.dea.Binder.DataSource;

import com.google.inject.Inject;
import oose.dea.Playlist.Domain.Playlist;
import oose.dea.Track.Domain.Track;
import oose.dea.datasource.DatabaseProperties;

import javax.inject.Singleton;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
@Singleton
public class BinderDAO implements IBinderDAO {
    DatabaseProperties database = new DatabaseProperties();
    private static Logger theLogger = Logger.getLogger("PlaylistDAO");

    @Inject
    public BinderDAO() {
        try {
            Class.forName(database.getDriver());
        } catch (ClassNotFoundException e) {
            theLogger.finest("Failed to get Driver");
        }
    }

    @Override
    public void bind(int playlistId, int trackId) {
        Connection connection = database.OpenConnection();
        try {
            PreparedStatement preparedStatmentInsert = connection.prepareStatement("INSERT INTO playlist_track_binder (idPlaylist, idTrack) VALUES (?,?)");
            preparedStatmentInsert.setInt(1, playlistId);
            preparedStatmentInsert.setInt(2, trackId);
            preparedStatmentInsert.executeUpdate();
        } catch (SQLException e) {
            theLogger.severe("INSERT Failed: " + e.getErrorCode());
        }
    }

    @Override
    public List<Track> find(int playlistId) {
        Connection connection = database.OpenConnection();
        List<Track> trackList = new ArrayList<>();
        ResultSet resultSet;
        try {
            PreparedStatement preparedStatmentInsert = connection.prepareStatement("SELECT * FROM track INNER JOIN playlist_track_binder on (track.idTrack = playlist_track_binder.idTrack) Where playlist_track_binder.idPlaylist = ?");
            preparedStatmentInsert.setInt(1, playlistId);
            resultSet = preparedStatmentInsert.executeQuery();
            while (resultSet.next()) {
                Track track = new Track(resultSet.getInt("idTrack"), resultSet.getString("performer"), resultSet.getString("title"), resultSet.getString("url"), resultSet.getInt("duration"));
                trackList.add(track);
            }
        } catch (SQLException e) {
            theLogger.severe("SELECT Failed: " + e.getErrorCode());
        }
        return trackList;
    }
}
