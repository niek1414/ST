package oose.dea.Track.Domain;

public class Track {
    private int idTrack;
    private String performer;
    private String title;
    private String url;
    private int duration;

    public Track(int idTrack, String performer, String title, String url, int duration) {
        this.idTrack = idTrack;
        this.performer = performer;
        this.title = title;
        this.url = url;
        this.duration = duration;
    }

    public int getIdTrack() {
        return idTrack;
    }

    public String getPerformer() {
        return performer;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public int getDuration() {
        return duration;
    }
}
