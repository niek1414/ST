package oose.dea.Track.Presetation;

import com.google.inject.Inject;
import oose.dea.Playlist.Domain.Playlist;
import oose.dea.Playlist.Service.IManagePlaylistsService;
import oose.dea.Playlist.Service.ManagePlaylistsService;

import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/choosePlaylists")
@Singleton
public class ChoosePlaylistController extends HttpServlet {
    @Inject
    private IManagePlaylistsService IManagePlaylistsService;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Playlist> playlists = IManagePlaylistsService.getPlaylistsByOwner(request.getSession().getAttribute("naam").toString());
        request.setAttribute("all", playlists);
        request.setAttribute("track", request.getParameter("track"));
        request.getRequestDispatcher("choosePlaylists.jsp").forward(request, response);
    }
}
