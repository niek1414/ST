package oose.dea.Track.Service;

import com.google.inject.Inject;
import oose.dea.Track.DataSource.ITrackDAO;
import oose.dea.Track.DataSource.TrackDAO;
import oose.dea.Track.Domain.Track;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
@Singleton
public class SearchTrackService implements ISearchTrackService {
    @Inject
    ITrackDAO ITrackDAO;

    @Override
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Track> getTracks() {
        return ITrackDAO.list();
    }

    @Override
    @GET
    @Path("/{title}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Track> getTracksByTitle(@PathParam("title") final String title) {
        return ITrackDAO.findByTitle(title);
    }
}
