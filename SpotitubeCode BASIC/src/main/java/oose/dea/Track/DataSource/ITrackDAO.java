package oose.dea.Track.DataSource;

import oose.dea.Track.Domain.Track;

import java.util.List;

public interface ITrackDAO {
    List<Track> list();

    List<Track> findByTitle(String title);
}
