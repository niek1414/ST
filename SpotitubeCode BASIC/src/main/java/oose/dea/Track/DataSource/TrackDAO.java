package oose.dea.Track.DataSource;

import com.google.inject.Inject;
import oose.dea.Track.Domain.Track;
import oose.dea.datasource.DatabaseProperties;

import javax.inject.Singleton;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
@Singleton
public class TrackDAO implements ITrackDAO {

    DatabaseProperties database = new DatabaseProperties();
    private static Logger theLogger = Logger.getLogger("TrackDAO");

    @Inject
    public TrackDAO() {
        try {
            Class.forName(database.getDriver());
        } catch (ClassNotFoundException e) {
            theLogger.finest("Failed to get Driver");
        }
    }

    @Override
    public List<Track> list() {
        Connection connection = database.OpenConnection();
        ResultSet resultSet;
        List<Track> result = new ArrayList<Track>();
        try {
            PreparedStatement preparedStatmentInsert = connection.prepareStatement("SELECT * FROM track");
            resultSet = preparedStatmentInsert.executeQuery();
            while (resultSet.next()) {
                Track track = new Track(resultSet.getInt("idTrack"), resultSet.getString("performer"), resultSet.getString("title"), resultSet.getString("url"), resultSet.getInt("duration"));
                result.add(track);
            }
            connection.close();
        } catch (SQLException e) {
            theLogger.severe("Retrieving List of tracks failed" + e);
        }
        return result;
    }

    @Override
    public List<Track> findByTitle(String title) {
        Connection connection = database.OpenConnection();
        ResultSet resultSet;
        List<Track> result = new ArrayList<Track>();
        try {
            PreparedStatement preparedStatmentInsert = connection.prepareStatement("SELECT * FROM track WHERE title LIKE ? ");
            preparedStatmentInsert.setString(1, "%" + title + "%");
            resultSet = preparedStatmentInsert.executeQuery();
            while (resultSet.next()) {
                Track track = new Track(resultSet.getInt("idTrack"), resultSet.getString("performer"), resultSet.getString("title"), resultSet.getString("url"), resultSet.getInt("duration"));
                result.add(track);
            }
            connection.close();
        } catch (SQLException e) {
            theLogger.severe("Retrieving List of tracks by title failed" + e);
        }
        return result;
    }

}
