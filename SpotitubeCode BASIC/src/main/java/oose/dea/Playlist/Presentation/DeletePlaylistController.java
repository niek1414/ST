package oose.dea.Playlist.Presentation;

import com.google.inject.Inject;
import oose.dea.Playlist.Service.IManagePlaylistsService;
import oose.dea.Playlist.Service.ManagePlaylistsService;

import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/deletePlaylist")
@Singleton
public class DeletePlaylistController extends HttpServlet {
    @Inject
    private IManagePlaylistsService IManagePlaylistsService;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int playlistid = Integer.parseInt(request.getParameter("playlist"));
        IManagePlaylistsService.deletePlaylist(playlistid);
        request.getRequestDispatcher("deletePlaylist.jsp").forward(request, response);
    }
}