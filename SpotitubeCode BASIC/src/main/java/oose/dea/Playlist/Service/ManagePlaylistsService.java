package oose.dea.Playlist.Service;

import com.google.inject.Inject;
import oose.dea.Playlist.DataSource.IPlaylistDAO;
import oose.dea.Playlist.DataSource.PlaylistDAO;
import oose.dea.Playlist.Domain.Playlist;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/playlist")
public class ManagePlaylistsService implements IManagePlaylistsService {

    @Inject
    IPlaylistDAO IPlaylistDAO;

    @Override
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addPlaylist(Playlist playlist){
        IPlaylistDAO.save(playlist);
    }

    @Override
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Playlist> getPlaylists() {
        return IPlaylistDAO.list();
    }

    @Override
    @GET
    @Path("/{owner}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Playlist> getPlaylistsByOwner(@PathParam("owner") final String owner) {
        return IPlaylistDAO.findByOwner(owner);
    }

    @Override
    @DELETE
    @Path("/{playlistId}")
    public void deletePlaylist(@PathParam("playlistId") final int playlistId)
    {
        IPlaylistDAO.delete(playlistId);
    }

    @Override
    @PUT
    @Path("/{playlistId}/{name}")
    public void updatePlaylist(@PathParam("playlistId") final int playlistId, @PathParam("name") String name){
        IPlaylistDAO.edit(playlistId,name);
    }
}
