package oose.dea.Playlist.Service;

import oose.dea.Playlist.Domain.Playlist;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

public interface IManagePlaylistsService {
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    void addPlaylist(Playlist playlist);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<Playlist> getPlaylists();

    @GET
    @Path("/{owner}")
    @Produces(MediaType.APPLICATION_JSON)
    List<Playlist> getPlaylistsByOwner(@PathParam("owner") String owner);

    @DELETE
    @Path("/{playlistId}")
    void deletePlaylist(@PathParam("playlistId") int playlistId);

    @PUT
    @Path("/{playlistId}/{name}")
    void updatePlaylist(@PathParam("playlistId") int playlistId, @PathParam("name") String name);
}
