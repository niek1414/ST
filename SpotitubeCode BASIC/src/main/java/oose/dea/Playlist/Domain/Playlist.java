package oose.dea.Playlist.Domain;

public class Playlist {
    private int idPlaylist;
    private String owner;
    private String name;

    public Playlist(int idPlaylist, String owner, String name) {
        this.idPlaylist = idPlaylist;
        this.owner = owner;
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public String getName() {
        return name;
    }

    public int getIdPlaylist() {
        return idPlaylist;
    }
}
