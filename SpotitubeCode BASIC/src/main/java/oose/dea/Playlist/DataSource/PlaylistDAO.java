package oose.dea.Playlist.DataSource;

import com.google.inject.Inject;
import oose.dea.Playlist.Domain.Playlist;
import oose.dea.datasource.DatabaseProperties;

import javax.inject.Singleton;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
@Singleton
public class PlaylistDAO implements IPlaylistDAO {

    DatabaseProperties database = new DatabaseProperties();
    private static Logger theLogger = Logger.getLogger("PlaylistDAO");

    @Inject
    public PlaylistDAO() {
        try {
            Class.forName(database.getDriver());
        } catch (ClassNotFoundException e) {
            theLogger.finest("Failed to get Driver");
        }
    }


    @Override
    public List<Playlist> list() {
        Connection connection = database.OpenConnection();
        ResultSet resultSet;
        List<Playlist> result = new ArrayList<Playlist>();
        try {
            PreparedStatement preparedStatmentInsert = connection.prepareStatement("SELECT * FROM playlist");
            resultSet = preparedStatmentInsert.executeQuery();
            while (resultSet.next()) {
                Playlist playlist = new Playlist(resultSet.getInt("idPlaylist"), resultSet.getString("owner"), resultSet.getString("name"));
                result.add(playlist);
            }
            connection.close();
        } catch (SQLException e) {
            theLogger.severe("SELECT failed");
        }
        return result;
    }

    @Override
    public List<Playlist> findByOwner(String owner) {
        Connection connection = database.OpenConnection();
        ResultSet resultSet;
        List<Playlist> result = new ArrayList<Playlist>();
        try {
            PreparedStatement preparedStatmentInsert = connection.prepareStatement("SELECT * FROM playlist WHERE owner LIKE ?");
            preparedStatmentInsert.setString(1, "%" + owner + "%");
            resultSet = preparedStatmentInsert.executeQuery();
            while (resultSet.next()) {
                Playlist playlist = new Playlist(resultSet.getInt("idPlaylist"), resultSet.getString("owner"), resultSet.getString("name"));
                result.add(playlist);
            }
            connection.close();
        } catch (SQLException e) {
            theLogger.severe("Retrieving List of Playlists failed");
        }
        return result;
    }

    @Override
    public void save(Playlist playlist) {
        Connection connection = database.OpenConnection();
        try {
            PreparedStatement preparedStatmentInsert = connection.prepareStatement("INSERT INTO playlist (owner, name) VALUES (?,?)");
            preparedStatmentInsert.setString(1, playlist.getOwner());
            preparedStatmentInsert.setString(2, playlist.getName());
            preparedStatmentInsert.executeUpdate();
        } catch (SQLException e) {
            theLogger.severe("INSERT Failed: " + e.getErrorCode());
        }
    }

    @Override
    public void delete(int playlistId) {
        Connection connection = database.OpenConnection();
        try {
            PreparedStatement preparedStatmentInsert = connection.prepareStatement("DELETE FROM playlist WHERE idPlaylist = ?");
            preparedStatmentInsert.setInt(1, playlistId);
            preparedStatmentInsert.executeUpdate();
        } catch (SQLException e) {
            theLogger.severe("Deletion of playlist Failed: " + e.getErrorCode());
        }
    }

    @Override
    public void edit(int playlistId, String name) {
        Connection connection = database.OpenConnection();
        try {
            PreparedStatement preparedStatmentInsert = connection.prepareStatement("UPDATE playlist SET name=? WHERE idPlaylist = ?");
            preparedStatmentInsert.setString(1, name);
            preparedStatmentInsert.setInt(2, playlistId);
            preparedStatmentInsert.executeUpdate();
        } catch (SQLException e) {
            theLogger.severe("Update of playlist Failed: " + e.getErrorCode());
        }
    }
}
