package oose.dea.Playlist.DataSource;

import oose.dea.Playlist.Domain.Playlist;

import java.util.List;

public interface IPlaylistDAO {
    List<Playlist> list();

    List<Playlist> findByOwner(String owner);

    void save(Playlist playlist);

    void delete(int playlistId);

    void edit(int playlistId, String name);
}
