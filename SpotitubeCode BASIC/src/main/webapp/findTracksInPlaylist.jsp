<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Daymian
  Date: 13-10-2015
  Time: 09:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Spotitube</title>
</head>
<body>

<table>
  <thead>
  <th>Artiest</th>
  <th>Titel</th>
  <th>URL</th>
  <th>Duur</th>
  </thead>
  <tbody>
  <c:forEach items="${all}" var="current">
    <tr>
      <td>
        <c:out value="${current.performer}"/>
      </td>
      <td>
        <c:out value="${current.title}"/>
      </td>
      <td>
        <c:out value="${current.url}"/>
      </td>
      <td>
        <c:out value="${current.duration}"/>
      </td>
    </tr>
  </c:forEach>
  </tbody>
</table>
<form action="/findPlaylists">
  <input type="submit" value="Ga terug">
</form>
</body>
</html>