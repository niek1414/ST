<%--
  Created by IntelliJ IDEA.
  User: Daymian
  Date: 13-10-2015
  Time: 09:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String name;
    if (session.getAttribute("start") == null) {
        name = request.getParameter("naam");
        session.setAttribute("naam", name);
        session.setAttribute("start", true);
    } else {
        name = (String)session.getAttribute("naam");
    }

%>

<html>
<head>
    <title>Spotitube</title>
</head>
<body>
<h3>Spotitube menu</h3>
<p>Welkom <%=name%></p>

<form action="/findPlaylists">
<input type="submit" value="Playlists beheren">
</form>

<form action="/findTrack">
  <input type="submit" value="Tracks zoeken">
</form>

<form action="/logout">
<input type="submit" value="Log uit" name="logout">
</form>

</body>
</html>
