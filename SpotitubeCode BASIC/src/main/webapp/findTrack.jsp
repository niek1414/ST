<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Spotitube</title>
</head>
<body>
<form method="post" action="/findTrack">
  <p>Track naam: <input type="text" name="search"> <input type="submit" value="Zoek"></p>
</form>

<table>
  <thead>
  <th>Artiest</th>
  <th>Titel</th>
  <th>URL</th>
  <th>Duur</th>
  </thead>
  <tbody>
  <c:forEach items="${all}" var="current">
    <tr>
      <td>
        <c:out value="${current.performer}"/>
      </td>
      <td>
        <c:out value="${current.title}"/>
      </td>
      <td>
        <c:out value="${current.url}"/>
      </td>
      <td>
        <c:out value="${current.duration}"/>
      </td>
      <td>
        <form method="post" action="/choosePlaylists">
          <input type="hidden" name="track" value="${current.idTrack}">
          <input type="submit" name="bind" value="Voeg toe aan playlist...">
        </form>
      </td>
    </tr>
  </c:forEach>
  </tbody>
</table>
<form action="menu.jsp">
  <input type="submit" value="Ga terug">
</form>
</body>
</html>