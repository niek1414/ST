<%--
  Created by IntelliJ IDEA.
  User: Daymian
  Date: 13-10-2015
  Time: 10:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Spotitube</title>
</head>
<body>
<table>
  <thead>
  <th>Eigenaar</th>
  <th>Naam</th>
  </thead>
  <tbody>
  <c:forEach items="${all}" var="current">
    <tr>
      <td>
        <c:out value="${current.owner}"/>
      </td>
      <td>
        <c:out value="${current.name}"/>
      </td>
      <td>
        <form method="post" action="/binder">
          <input type="hidden" name="track" value="<%=request.getAttribute("track")%>">
          <input type="hidden" name="playlist" value="${current.idPlaylist}">
          <input type="submit" name="bind" value="Kies playlist"></form>
      </td>
    </tr>
  </c:forEach>
  </tbody>
</table>
<form action="findTrack.jsp">
  <input type="submit" value="Ga terug">
</form>
</body>
</html>
