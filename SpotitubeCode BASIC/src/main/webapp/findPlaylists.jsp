<%--
  Created by IntelliJ IDEA.
  User: Daymian
  Date: 13-10-2015
  Time: 10:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Spotitube</title>
</head>
<body>
<form method="post" action="/addPlaylist">
  <p>Playlistnaam: <input type="text" name="playlistnaam"> <input type="submit" value="Nieuwe playlist toevoegen"></p>
</form>
<table>
  <thead>
  <th>Eigenaar</th>
  <th>Naam</th>
  </thead>
  <tbody>
  <c:forEach items="${all}" var="current">
    <tr>
      <td>
        <c:out value="${current.owner}"/>
      </td>
      <td>
        <c:out value="${current.name}"/>
      </td>
      <td>
        <form method="post" action="/deletePlaylist"><input type="hidden" name="playlist" value="${current.idPlaylist}"><input type="submit" name="delete" value="Verwijder"></form>
      </td>
      <td>
        <form method="post" action="/updatePlaylist">
          <input type="hidden" name="playlist" value="${current.idPlaylist}">
          <p>
            Nieuwe playlistnaam: <input type="text" name="name">
            <input type="submit" value="playlist wijzigen">
          </p>
        </form>
      </td>
      <td>
        <form method="post" action="/findTracksInPlaylist">
          <input type="hidden" name="playlist" value="${current.idPlaylist}">
            <input type="submit" value="Tracks in deze playlist laten zien.">
        </form>
      </td>
    </tr>
  </c:forEach>
  </tbody>
</table>
<form action="menu.jsp">
  <input type="submit" value="Ga terug">
</form>
</body>
</html>
